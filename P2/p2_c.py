def checkPallindrome(str2):
    str1 = (str2.replace(" ","")).lower()
    exclude = [' ',',','/','!','@','$']
    str1REV = str1[::-1]
    
    for x in range(len(str1)):
        if str1[x] in exclude or str1REV[x] in exclude:
            continue 
        elif str1[x] != str1REV[x]:
            return False            
    return True

print(checkPallindrome("Lisa bonet ate no basil"))