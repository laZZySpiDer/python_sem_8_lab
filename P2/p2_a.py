def translate():
    user_input = input("Enter a string : ")
    temp = []
    vowels = ['a','e','i','o','u','A','E','I','O','U']
    for x in user_input:
        if x == " ":
            temp.append(" ")
        elif x not in vowels:
            temp.append(x)
            temp.append("O")
            temp.append(x)
        else :
            temp.append(x)
    result = ''.join(temp)
    print(result)
    
translate()