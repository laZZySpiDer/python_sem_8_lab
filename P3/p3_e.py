def check_pwd_strength(password_user):
    special_symbol = ['!','@','#','$','%','^','&','*','(',')']
    alphabets = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
    alpha = 0
    sym = 0
    if(len(password_user) >= 8):
        for x in password_user:
            if x in alphabets:
                alpha += 1
            if x in special_symbol:
                sym += 1
        if alpha < 2 or sym < 2 :
            return "Password is Weak"
        else:
            return "Password is Strong"
    else:
        return "Password is Weak"        

    



print(check_pwd_strength("ABasd!@1"))