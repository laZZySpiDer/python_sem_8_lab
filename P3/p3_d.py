def make_ing_form(verb):
    
    vowel = ['a','e','i','o','u','A','E','I','O','U']
    
    if(verb[-1] == 'e' and verb[-2] != 'i'):
        result = verb[:-1] + "ing"
        print(result)

    if(verb[-1] == 'e' and verb[-2] == 'i' ):
        result = verb[:-2] + "ying"
        print(result)

    if(verb[-1] not in vowel and verb[-2] in vowel and verb[-3] not in vowel):
        result = verb + verb[-1] + "ing" 
        print(result)

make_ing_form("hug")