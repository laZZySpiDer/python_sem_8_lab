def checkPangram(str1):
    str1 = str1.lower()
    alphabets = [" ",'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
    for x in alphabets:
        if x not in str1:
            return False

    return True


print(checkPangram("The quick brown fox jumps over the lazy dog"))